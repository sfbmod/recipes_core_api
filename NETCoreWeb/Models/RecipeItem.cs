﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Recipes.Models
{
    public class RecipeItem
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string PictureURL { get; set; }
        public string Notes { get; set; }
        public int Ingredient1 { get; set; }
        public int Ingredient2 { get; set; }
        public int Ingredient3 { get; set; }
    }
}