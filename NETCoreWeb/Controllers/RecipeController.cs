﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Recipes.Models;

namespace Recipes.Controllers
{
    [Route("api/todo")]
    [ApiController]
    public class RecipeController : ControllerBase
    {
        private readonly RecipeContext _context;

        public RecipeController(RecipeContext context)
        {
            _context = context;

            if (_context.RecipeItems.Count() == 0)
            {
                // Create a new RecipeItem if collection is empty,
                // which means you can't delete all RecipeItems.
                _context.RecipeItems.Add(new RecipeItem { Name = "Chicken Soup" });
                _context.SaveChanges();
            }
        }
        
        // GET: api/Recipe
        [HttpGet]
        public async Task<ActionResult<IEnumerable<RecipeItem>>> GetRecipeItems()
        { return await _context.RecipeItems.ToListAsync(); }

        // GET: api/Recipe/5
        [HttpGet("{id}")]
        public async Task<ActionResult<RecipeItem>> GetRecipeItem(int id)
        {
            var recipeItem = await _context.RecipeItems.FindAsync(id);

            if (recipeItem == null)
            { return NotFound(); }

            return recipeItem;
        }

        // POST: api/Recipe
        [HttpPost]
        public async Task<ActionResult<RecipeItem>> PostTodoItem(RecipeItem recipeItem)
        {
            _context.RecipeItems.Add(recipeItem);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetRecipeItem", new { id = recipeItem.ID }, recipeItem);
        }

        // PUT: api/Recipe/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutRecipeItem(long id, RecipeItem recipeItem)
        {
            if (id != recipeItem.ID)
            { return BadRequest(); }

            _context.Entry(recipeItem).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            return NoContent();
        }

        // DELETE: api/Recipe/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<RecipeItem>> DeleteRecipeItem(long id)
        {
            var recipeItem = await _context.RecipeItems.FindAsync(id);
            if (recipeItem == null)
            { return NotFound(); }

            _context.RecipeItems.Remove(recipeItem);
            await _context.SaveChangesAsync();

            return recipeItem;
        }
    }
}